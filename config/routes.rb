Rails.application.routes.draw do

  root 'attendances#index'
  
  devise_for :users
  resources :users
  resources :receipts
  resources :enterprises
  resources :attendances
  resources :items
  resources :device_ids
  resources :account_types
  resources :payment_methods
  resources :attendance_statuses
  resources :attendance_types
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
