class CreateReceipts < ActiveRecord::Migration[5.0]
  def change
    create_table :receipts do |t|
      t.datetime :date
      t.string :address
      t.integer :vendor_id
      t.integer :client_id
      t.decimal :total
      t.references :item, foreign_key: true
      t.references :enterprise, foreign_key: true

      t.timestamps
    end
  end
end
