class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :password
      t.boolean :current_user
      t.string :telephone
      t.string :zip_code
      t.string :address
      t.boolean :facebook_account
      t.string :facebook_token
      t.boolean :active
      t.boolean :blocked
      t.string :cpf
      t.string :cnpj
      t.references :account_type, foreign_key: true
      t.references :enterprise, foreign_key: true

      t.timestamps
    end
  end
end
