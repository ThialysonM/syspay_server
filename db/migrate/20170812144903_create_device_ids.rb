class CreateDeviceIds < ActiveRecord::Migration[5.0]
  def change
    create_table :device_ids do |t|
      t.string :token
      t.string :os
      t.string :session_id

      t.timestamps
    end
  end
end
