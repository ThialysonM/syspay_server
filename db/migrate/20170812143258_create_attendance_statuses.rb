class CreateAttendanceStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :attendance_statuses do |t|
      t.string :name

      t.timestamps
    end
  end
end
