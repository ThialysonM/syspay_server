class CreateAttendances < ActiveRecord::Migration[5.0]
  def change
    create_table :attendances do |t|
      t.datetime :date
      t.references :attendance_status, foreign_key: true
      t.references :attendance_type, foreign_key: true
      t.references :payment_method, foreign_key: true
      t.integer :vendor_id
      t.integer :client_id
      t.references :item, foreign_key: true
      t.references :receipts, foreign_key: true
      t.decimal :total

      t.timestamps
    end
  end
end
