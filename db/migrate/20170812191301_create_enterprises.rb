class CreateEnterprises < ActiveRecord::Migration[5.0]
  def change
    create_table :enterprises do |t|
      t.string :name
      t.string :cnpj
      t.string :cpf
      t.string :address
      t.string :email
      t.string :telephone

      t.timestamps
    end
  end
end
