# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181024142733) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attendance_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attendance_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attendances", force: :cascade do |t|
    t.datetime "date"
    t.integer  "attendance_status_id"
    t.integer  "attendance_type_id"
    t.integer  "payment_method_id"
    t.integer  "vendor_id"
    t.integer  "client_id"
    t.integer  "item_id"
    t.integer  "receipts_id"
    t.decimal  "total"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["attendance_status_id"], name: "index_attendances_on_attendance_status_id", using: :btree
    t.index ["attendance_type_id"], name: "index_attendances_on_attendance_type_id", using: :btree
    t.index ["item_id"], name: "index_attendances_on_item_id", using: :btree
    t.index ["payment_method_id"], name: "index_attendances_on_payment_method_id", using: :btree
    t.index ["receipts_id"], name: "index_attendances_on_receipts_id", using: :btree
  end

  create_table "device_ids", force: :cascade do |t|
    t.string   "token"
    t.string   "os"
    t.string   "session_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "enterprises", force: :cascade do |t|
    t.string   "name"
    t.string   "cnpj"
    t.string   "cpf"
    t.string   "address"
    t.string   "email"
    t.string   "telephone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "items", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "quantity"
    t.decimal  "unit_price"
    t.decimal  "total_price"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "payment_methods", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "receipts", force: :cascade do |t|
    t.datetime "date"
    t.string   "address"
    t.integer  "vendor_id"
    t.integer  "client_id"
    t.decimal  "total"
    t.integer  "item_id"
    t.integer  "enterprise_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["enterprise_id"], name: "index_receipts_on_enterprise_id", using: :btree
    t.index ["item_id"], name: "index_receipts_on_item_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "password"
    t.boolean  "current_user"
    t.string   "telephone"
    t.string   "zip_code"
    t.string   "address"
    t.boolean  "facebook_account"
    t.string   "facebook_token"
    t.boolean  "active"
    t.boolean  "blocked"
    t.string   "cpf"
    t.string   "cnpj"
    t.integer  "account_type_id"
    t.integer  "enterprise_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.index ["account_type_id"], name: "index_users_on_account_type_id", using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["enterprise_id"], name: "index_users_on_enterprise_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "attendances", "attendance_statuses"
  add_foreign_key "attendances", "attendance_types"
  add_foreign_key "attendances", "items"
  add_foreign_key "attendances", "payment_methods"
  add_foreign_key "attendances", "receipts", column: "receipts_id"
  add_foreign_key "receipts", "enterprises"
  add_foreign_key "receipts", "items"
  add_foreign_key "users", "account_types"
  add_foreign_key "users", "enterprises"
end
