json.extract! user, :id, :name, :email, :password, :current_user, :telephone, :zip_code, :address, :facebook_account, :facebook_token, :active, :blocked, :account_type_id, :cpf, :cnpj, :enterprise_id, :created_at, :updated_at
json.url user_url(user, format: :json)
