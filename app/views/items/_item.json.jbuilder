json.extract! item, :id, :title, :description, :quantity, :unit_price, :total_price, :created_at, :updated_at
json.url item_url(item, format: :json)
