json.extract! attendance_status, :id, :name, :string, :created_at, :updated_at
json.url attendance_status_url(attendance_status, format: :json)
