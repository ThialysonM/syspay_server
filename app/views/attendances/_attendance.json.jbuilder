json.extract! attendance, :id, :date, :attendance_status_id, :attendance_type_id, :payment_method_id, :vendor_id, :client_id, :item_id, :receipts_id, :total, :created_at, :updated_at
json.url attendance_url(attendance, format: :json)
