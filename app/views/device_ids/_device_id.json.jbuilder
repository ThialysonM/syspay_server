json.extract! device_id, :id, :token, :os, :session_id, :created_at, :updated_at
json.url device_id_url(device_id, format: :json)
