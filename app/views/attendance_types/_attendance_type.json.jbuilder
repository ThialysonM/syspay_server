json.extract! attendance_type, :id, :name, :string, :created_at, :updated_at
json.url attendance_type_url(attendance_type, format: :json)
