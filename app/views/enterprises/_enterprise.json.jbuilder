json.extract! enterprise, :id, :name, :cnpj, :cpf, :address, :email, :telephone, :created_at, :updated_at
json.url enterprise_url(enterprise, format: :json)
