json.extract! receipt, :id, :date, :address, :vendor_id, :client_id, :total, :item_id, :enterprise_id, :created_at, :updated_at
json.url receipt_url(receipt, format: :json)
