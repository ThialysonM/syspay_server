class AttendanceStatusesController < ApplicationController
  before_action :set_attendance_status, only: [:show, :edit, :update, :destroy]

  # GET /attendance_statuses
  # GET /attendance_statuses.json
  def index
    @attendance_statuses = AttendanceStatus.all
  end

  # GET /attendance_statuses/1
  # GET /attendance_statuses/1.json
  def show
  end

  # GET /attendance_statuses/new
  def new
    @attendance_status = AttendanceStatus.new
  end

  # GET /attendance_statuses/1/edit
  def edit
  end

  # POST /attendance_statuses
  # POST /attendance_statuses.json
  def create
    @attendance_status = AttendanceStatus.new(attendance_status_params)

    respond_to do |format|
      if @attendance_status.save
        format.html { redirect_to @attendance_status, notice: 'Attendance status was successfully created.' }
        format.json { render :show, status: :created, location: @attendance_status }
      else
        format.html { render :new }
        format.json { render json: @attendance_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /attendance_statuses/1
  # PATCH/PUT /attendance_statuses/1.json
  def update
    respond_to do |format|
      if @attendance_status.update(attendance_status_params)
        format.html { redirect_to @attendance_status, notice: 'Attendance status was successfully updated.' }
        format.json { render :show, status: :ok, location: @attendance_status }
      else
        format.html { render :edit }
        format.json { render json: @attendance_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /attendance_statuses/1
  # DELETE /attendance_statuses/1.json
  def destroy
    @attendance_status.destroy
    respond_to do |format|
      format.html { redirect_to attendance_statuses_url, notice: 'Attendance status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_attendance_status
      @attendance_status = AttendanceStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def attendance_status_params
      params.require(:attendance_status).permit(:name, :string)
    end
end
