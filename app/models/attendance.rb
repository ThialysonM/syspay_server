class Attendance < ApplicationRecord
  belongs_to :attendance_status
  belongs_to :attendance_type
  belongs_to :payment_method
  has_many :item
  has_one :receipts
end
