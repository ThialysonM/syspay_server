class Receipt < ApplicationRecord
  belongs_to :item
  belongs_to :enterprise
  belongs_to :attendance
end
