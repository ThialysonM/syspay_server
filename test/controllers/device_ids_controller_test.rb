require 'test_helper'

class DeviceIdsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @device_id = device_ids(:one)
  end

  test "should get index" do
    get device_ids_url
    assert_response :success
  end

  test "should get new" do
    get new_device_id_url
    assert_response :success
  end

  test "should create device_id" do
    assert_difference('DeviceId.count') do
      post device_ids_url, params: { device_id: { os: @device_id.os, session_id: @device_id.session_id, token: @device_id.token } }
    end

    assert_redirected_to device_id_url(DeviceId.last)
  end

  test "should show device_id" do
    get device_id_url(@device_id)
    assert_response :success
  end

  test "should get edit" do
    get edit_device_id_url(@device_id)
    assert_response :success
  end

  test "should update device_id" do
    patch device_id_url(@device_id), params: { device_id: { os: @device_id.os, session_id: @device_id.session_id, token: @device_id.token } }
    assert_redirected_to device_id_url(@device_id)
  end

  test "should destroy device_id" do
    assert_difference('DeviceId.count', -1) do
      delete device_id_url(@device_id)
    end

    assert_redirected_to device_ids_url
  end
end
