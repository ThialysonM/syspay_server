require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get users_url
    assert_response :success
  end

  test "should get new" do
    get new_user_url
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post users_url, params: { user: { account_type_id: @user.account_type_id, active: @user.active, address: @user.address, blocked: @user.blocked, cnpj: @user.cnpj, cpf: @user.cpf, current_user: @user.current_user, email: @user.email, enterprise_id: @user.enterprise_id, facebook_account: @user.facebook_account, facebook_token: @user.facebook_token, name: @user.name, password: @user.password, telephone: @user.telephone, zip_code: @user.zip_code } }
    end

    assert_redirected_to user_url(User.last)
  end

  test "should show user" do
    get user_url(@user)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_url(@user)
    assert_response :success
  end

  test "should update user" do
    patch user_url(@user), params: { user: { account_type_id: @user.account_type_id, active: @user.active, address: @user.address, blocked: @user.blocked, cnpj: @user.cnpj, cpf: @user.cpf, current_user: @user.current_user, email: @user.email, enterprise_id: @user.enterprise_id, facebook_account: @user.facebook_account, facebook_token: @user.facebook_token, name: @user.name, password: @user.password, telephone: @user.telephone, zip_code: @user.zip_code } }
    assert_redirected_to user_url(@user)
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete user_url(@user)
    end

    assert_redirected_to users_url
  end
end
