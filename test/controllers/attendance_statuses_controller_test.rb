require 'test_helper'

class AttendanceStatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @attendance_status = attendance_statuses(:one)
  end

  test "should get index" do
    get attendance_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_attendance_status_url
    assert_response :success
  end

  test "should create attendance_status" do
    assert_difference('AttendanceStatus.count') do
      post attendance_statuses_url, params: { attendance_status: { name: @attendance_status.name, string: @attendance_status.string } }
    end

    assert_redirected_to attendance_status_url(AttendanceStatus.last)
  end

  test "should show attendance_status" do
    get attendance_status_url(@attendance_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_attendance_status_url(@attendance_status)
    assert_response :success
  end

  test "should update attendance_status" do
    patch attendance_status_url(@attendance_status), params: { attendance_status: { name: @attendance_status.name, string: @attendance_status.string } }
    assert_redirected_to attendance_status_url(@attendance_status)
  end

  test "should destroy attendance_status" do
    assert_difference('AttendanceStatus.count', -1) do
      delete attendance_status_url(@attendance_status)
    end

    assert_redirected_to attendance_statuses_url
  end
end
